
\documentclass[a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[english,czech]{babel}
\usepackage[usenames,dvipsnames]{xcolor}
\usepackage{hyperref}
\hypersetup{pdftex,
  colorlinks=false,
  urlbordercolor = black,
  %  pdfborder = 0 0 0,
  pdfauthor=Martina a Pavel,
  unicode=true,
  pdfborderstyle = {/S/U/W 1}
}
\usepackage[pdftex]{graphicx}
\usepackage{listings}

%% Just for marking parts of the text to be done or fixed
\newcommand{\alert}[1]{\textcolor{red}{#1}}

\addto\extrasczech{%
  \def\sectionautorefname{kapitola}%
  \def\subsectionautorefname{podkapitola}%
  \def\subsubsectionautorefname{podkapitola}%
}

\title{Breve}
\author{Martina Mejzlíková, Pavel Kačer}
\date{\today}

\begin{document}

\maketitle

\lstset{
  inputencoding = utf8,
  numbers       = left,
  frame         = single,
  showstringspaces = false,
  backgroundcolor = \color[rgb]{0.8,0.8,0.8},
}

\tableofcontents
\newpage

\section{Breve}
\label{sec:breve}

Breve je multiagentní simulující systém, který umožňuje sledovat
chování agentů v~3D modelu simulovaného prostředí. Jazyky používané
pro vytváření modelů jsou objektově orientované. Lze programovat
v~jazyku \href{https://www.python.org/}{Python} (viz
\autoref{sec:programovani-simulaci-v-pythonu}) nebo jazyku
\href{http://spiderland.org/node/2632}{Steve} (viz
\autoref{sec:zaklady-jazyka-steve}). Druhý jazyk je specificky vyvinut
pro tento systém vycházející syntaxí z~jazyka C.

Breve je GPL software distribuovaný ve dvou verzích. Verze s~grafickým
prostředím obsahuje editor pro vývojáře a object browser. Součástí
instalace jsou ukázkové (demo) projekty, které lze dále rozvíjet, nebo
použít vestavěné objekty ve vlastních projektech.


\section{Objekty v Breve}
\label{sec:objekty-v-breve}

V~breve zpravidla vytváříme dva typy objektů modelu, stacionární a
mobilní. Jádrem celého simulujícího systému je \emph{breve
  engine}. Mobilní agenty musí být schopni komunikovat s~engine breve,
které detekuje kolize s~okolím nebo jinými agenty a řídí jejich
reakce. Pro každý časový krok (časovou jednotku) simulace
implementovaného modelu má takový objekt definováno nějaké chování.

Pokud je vytvořen objekt (instance třídy), vyvolá se tzv. \emph{init
  událost}. To znamená, že je zavolána stejnojmenná metoda \verb!init!
vytvořeného objektu.  V~každé časové jednotce simulace je volána
\emph{iterate událost}. Je zavolána metoda \verb!iterate!, která
definuje chování objektu v~každé časové jednotce.

\begin{figure}[ht]\centering
  \includegraphics[width=4.06482in,height=2.43334in]{architecture.jpg}
  \caption{Architektura běhového prostředí Breve \cite{Klein2006}}
\label{fig:architecture}
\end{figure}


\section{Základy jazyka Steve}
\label{sec:zaklady-jazyka-steve}

\subsection{Definování třídy}

Základní třída, která by měla být rodičovskou třídou každé třídy, je
třída \emph{Objekt}. Každý potomek této třídy je zaregistrován breve
engine. Tato třída však nemá žádné zvláštní schopnosti.

\begin{lstlisting}
nazev_rodicovske_tridy: nazev_tridy {
}
\end{lstlisting}

Rodičovskou třídou \verb!Stationary! dědí objekty, které se nepohybují
-- objekty v~klidu. Stationary objekty jsou součástí prostředí,
ve~kterém s~nimi přicházejí do kontaktu mobilní agenty. Pro všechny
objekty, které se pohybují, dědí rodičovskou třídu \verb!Mobile!.
Mobile objekty se pohybující v~breve 3D světě díky zděděným
vlastnostem umí pracovat s~polohou, rychlostí a~zrychlením.

Pro~vytváření více instancí stejné třídy jedním příkazem, lze přidat
argument \verb!aka!.

\begin{lstlisting}[emph={aka},emphstyle=\underbar]
Mobile : agent (aka agenti) {
}
\end{lstlisting}

Instance pak vytváříme pomocí příkazu (instance objektů počtu n) :

\begin{lstlisting}[emph={new},emphstyle=\underbar]
n new objekt.
\end{lstlisting}

Každý příkaz končí tečkou (obdoba středníku v~jiných jazycích,
např. Java).


\subsection{Definice proměnných}
\label{sec:definice-promennych}

Deklarační část je uvedena znaménkem \verb!+! a~slovem
\verb!variables!  s~dvojtečkou a~končí deklarací metody nebo
ukončujícím znakem pro~konec třídy (složená pravá závorka).

\begin{lstlisting}[emph={variables},emphstyle=\underbar]
Mobile : agent {
  + variables:
    myInt, myOtherInt (int).
    myObject (object).
    myFloat (float).
    with-energy energy = 100 (int)
    with-radius radius = 10 (int)
    with-name name = "agent" (string):
}
\end{lstlisting}

Základní datové typy vycházejí z~jazyka C, jsou to: Int, float,
object, vector, matrix, list, string, hash, pointer, data, více viz
\url{http://www.spiderland.org/node/2644}

Proměnné lze deklarovat s~přiřazenou hodnotou \lstinline{myInt = 4}.
Pro řetězce se používají uvozovky.

Proměnné lze definovat také jako globální konstanty.

\begin{lstlisting}[emph={@define},emphstyle=\underbar]
@define nazev_konstanty hodnota.
\end{lstlisting}


\subsection{Metody}
\label{sec:metody}

Deklarace metod začíná znaménkem \verb!+! a
slovem \verb!to!, za kterým následuje název metody a
končí deklarací metody nebo ukončujícím znakem pro
konec třídy (složená pravá závorka):

\begin{lstlisting}[emph={to},emphstyle=\underbar]
+ to nazev_metody:
\end{lstlisting}

Metoda začínající znakem \verb!-! namísto \verb!+! je neveřejná.
Pokud deklarujeme metodu s~argumenty, jsou složeny ze třech částí.

\begin{lstlisting}
+ to nazev_metody klicove_slovo nazev_promenne (typ):
\end{lstlisting}

Více argumentů se odděluje pouze mezerou.

\begin{lstlisting}[showspaces=true]
+ to nazev_metody klicove_slovo nazev_promenne (typ)
klicove_slovo nazev_promenne (typ):
\end{lstlisting}

Za dvojtečkou následuje deklarace metody, která končí ukončením
deklarace třídy nebo počátečním znakem pro deklaraci další
metody. Metodu pak voláme dvojicí klíčové slovo nebo hodnota, přičemž
nezávisí na pořadí proměnných.

\begin{lstlisting}
trida nazev_metody klicove_slovo hodnota
klicove_slovo hodnota.
\end{lstlisting}

Metody mohou mít lokální proměnné, ty je však třeba deklarovat vždy
na~začátku metody.

\begin{lstlisting}
+ to nazev_metody klicove_slovo nazev_promenne (typ):
  promenna_metody1 (typ).
  promenna_metody2 (typ).
  promenna_metody3 = 10000.
\end{lstlisting}

Návratové hodnoty metod -- funkcí se píši za slovem return.

\begin{lstlisting}[emph={return},emphstyle=\underbar]
return promenna.
\end{lstlisting}

Přiřazení návratové hodnoty do proměnné pak vypadá takto.

\begin{lstlisting}
promenna = (self nazev_metody).
\end{lstlisting}

Metody mohou být deklarovány s~argumenty s~předdefinovanými hodnotami.

\begin{lstlisting}
+ to nazev_metody klicove_slovo nazev_promenne
  = (1, 0, 0) (vector).
\end{lstlisting}

Takové metody lze volat jen příkazem self, při volání lze argumenty
přepsat

\begin{lstlisting}[emph={self},emphstyle=\underbar]
self nazev_metody klicove_slovo 5 klicove_slovo "Alena".
\end{lstlisting}

Lze také volat metodu v~argumentu.

\begin{lstlisting}
self nazev_metody to (promenna nazev_metody).
\end{lstlisting}

Kromě nezbytných metod \verb!init! a~\verb!iterate!, lze definovat
další metody s~názvy \verb!_post_iterate_!, která je volána po
vykonání metody \verb!_iterate_! a \verb!destroy!, jenž je volána
bezprostředně před zrušením objektu.

Speciální volání interních metod breve má odlišnou syntaxi od
předchozího.

\begin{lstlisting}
nazev_metody(arg1,arg2, ... argN)
\end{lstlisting}

Příkaz \verb!super! volá metodu nadřazené třídy.


\subsection{Objekty}
\label{sec:objekty}

Deklarace objektů je totožná s~deklarací proměnných, jak bylo uvedeno
v~předchozí kapitole. Instance objektu se vytvoří pomocí příkazu
\verb!new!:

\begin{lstlisting}[emph={new},emphstyle=\underbar]
agent (object).

agent = new Agent.
new Agent.
agent = 100 new Agenti.
\end{lstlisting}

\bigskip

Pro uvolnění paměti slouží příkaz \verb!free!.

\begin{lstlisting}[emph={free},emphstyle=\underbar]
free instance.
\end{lstlisting}


\subsubsection{Speciální výrazy volání objektů}
\label{sec:specialni-vyrazy-volani-objektu}

\begin{description}
\item [Self (object)] -- tato instance (obdoba this)
\item [Super (object)] -- instance nadřazené třídy
\item [Controller (object)] -- reference na controller instance
\end{description}


\subsection{Náhodná čísla}

Náhodná čísla se uplatní v~mnoha projektech jako jsou projekty
využívající například evoluční algoritmy. V~ukázkových projektech je
velice populární Walker a jeho různé varianty. Agent se během několika
hodin naučí pohybovat po~všech čtyřech končetinách.

Pro generování náhodného čísla použijeme příkaz
\verb!random [vyraz]!.  Výrazem může být proměnná typu int, float
nebo vektor, v~tomto případě generujeme souřadnice.

\begin{lstlisting}[emph={random},emphstyle=\underbar]
random[(10, 10, 20)]
\end{lstlisting}

Vrací prvky (0-10, 0-10, 0-20). V~simulacích se často objevuje potřeba
umístit objekt do okolí počátku (0,0,0), takové souřadnice můžeme
generovat.

\begin{lstlisting}
random[(40, 40, 40)] - (20, 20, 20).
\end{lstlisting}

Tato konvence nám dává vektor s každým prvkem mezi -20 a 20.

Breve nastavuje základ podle aktuálního času počítače, na~kterém běží
simulace. To pro~běžné programy většinou postačuje. Pro~nastavení
náhodného základu pro~generování sekvence pseudonáhodných čísel
z~\verb!/dev/random! lze použít funkci
\verb!set-random-seed-from-dev-random!.

\subsection{Řídící struktury}
\label{sec:ridici-struktury}

\subsubsection{Podmínky}
\label{sec:podminky}

\begin{lstlisting}[emph={if,else},emphstyle=\underbar]
if podminka(==,!=,>=,>,&&,||,!): pokud_je_pravda
  [else pokud_neni_pravda]
if podminka: {
  prikaz.
  prikaz.
} else prikaz.
\end{lstlisting}

\subsubsection{Cykly}
\label{sec:cykly}

\begin{lstlisting}[emph={while},emphstyle=\underbar]
while je_pravdiva_podminka: {
  prikaz.
  x++.
}
\end{lstlisting}

\subsubsection{Foreach -- procházení kontejneru list}
\label{sec:prochazeni-seznamu}

\begin{lstlisting}[emph={foreach},emphstyle=\underbar]
foreach prvek_listu in promenna_typu_list: {
  prikaz.
}
\end{lstlisting}

\subsubsection{For}
\label{sec:for}

\begin{lstlisting}[emph={for},emphstyle=\underbar]
for i = 0, i < 30, i += 1: {
  prikaz.
}
\end{lstlisting}

\subsubsection{Import rodičovské třídy}
\label{sec:import-rodicovske-tridy}

Soubor s~rodičovskou třídou jednoduše importujeme pomocí příkazu
s~uvedení celého jmena souboru včetně přípony.

\begin{lstlisting}[emph={@include},emphstyle=\underbar]
@include "Control.tz".
\end{lstlisting}

Nebo pro vnitřní třídy breve.

\begin{lstlisting}[emph={@use},emphstyle=\underbar]
@use Control.
\end{lstlisting}

Výchozí adresář nastavíme příkazem

\begin{lstlisting}[emph={@path},emphstyle=\underbar]
@path "Users/jk/breve_classes".
\end{lstlisting}

\subsection{Speciální třídy vytvářející prostředí}
\label{sec:specialni-tridy-vytvarejici-prostredi}

\subsubsection{Floor}
\label{sec:floor}

Floor je speciální případ třídy \verb!Stationary!. Jedná se o kvádr
velikosti (1000, 5, 1000) umístěný na~souřadnicích (0, -2.5, 0) tak,
že rovina země je umístěna na Y = 0.

\subsubsection{Kamera}
\label{sec:kamera}

Pro~přístup k~vlastnostem a~ovládání kamery je třeba inkludovat její
třídu.

\begin{lstlisting}
@use Camera.
\end{lstlisting}

Poté je možné s~ní pohybovat, vypínat a zapínat pomocí dostupných
metod. Více v~dokumentaci
\url{http://www.spiderland.org/documentation/steveclasses/Camera.html}.


\section{Projekt: Braitenbergovo vozidlo (Braitenberg vehicle)}
\label{sec:projekt}

Jde o mobilního agenta se senzory. Lze jej lze implementovat jednoduše
ze~šablony \verb!BraitenbergTemplate.tz!.

Třída importuje třídu Braitengerg, která zároveň řeší nastavení kamery
a základního prostředí -- \verb!Floor!, po kterém se bude agent
pohybovat.

\begin{lstlisting}
@use Braitenberg.
\end{lstlisting}

První kód bude obsahovat definici kontroleru a následně jeho metody
init.

\begin{lstlisting}
Controller kontroler.

BraitenbergControl: kontroler {
   + variables:
   + to init:
}
\end{lstlisting}

Pokud budeme chtít pracovat s~proměnnými v~rámci celého modelu,
nejprve deklaruje proměnné v~sekci \verb!+ variables!, ty pak budeme
používat v~následující metodě init.

Nejprve sestavíme vozidlo. Vytvoříme tělo a zviditelníme jej metodou
\verb!watch!.

\begin{lstlisting}
+ variables:
vozidlo(object).

+ to init:
vozidlo = new BraitenbergVehicle.
self watch item vozidlo.
\end{lstlisting}

Vozidlo můžeme umístit v~souřadnicích osy, vycházíme z~toho, že střed
plochy je střed světa (0, 0, 0), výchozí rozměr plochy je (1000, 5,
1000).

\begin{lstlisting}
vozidlo move to (0, 1, 0).
\end{lstlisting}

Nyní vozidlo opatříme koly, která deklarujeme jak (object).

\begin{lstlisting}
+ variables:
vozidlo (object).
leveKolo, praveKolo (object).
leveKolo  = (vehicle add-wheel at (0, 0, -1.5)).
praveKolo = (vehicle add-wheel at (0, 0, 1.5)).
\end{lstlisting}

Dvě nebo třeba i čtyři (nesmíme zapomenout deklarovat všechna čtyři
kola).

\begin{lstlisting}
levePredniKolo   = (vehicle add-wheel at (1.5, 0, -1.5)).
pravePredniKolol = (vehicle add-wheel at (1.5, 0, 1.5)).
leveZadniKolo    = (vehicle add-wheel at (-1.5, 0, -1.5)).
praveZadniKolo   = (vehicle add-wheel at (-1.5, 0, 1.5)).
\end{lstlisting}

Vozidlo se zatím nepohybuje. Kolům můžeme přiřadit počáteční rychlost,
kterou se začne vozidlo pohybovat. Rychlost může být rozdílná pro
levou a pravou stranu.

\begin{lstlisting}
leveKolo set-natural-velocity to 100.
praveKolo set-natural-velocity to 10.
\end{lstlisting}

Nyní se pohybuje, ale bezcílně. Cílem bylo vytvořit agenta. Ten by měl
reagovat na okolí. Braitenbergovo vozidlo může být vybaveno senzory,
které reagují na stacionární světelné body. Dalším krokem je tedy
přidání senzorů, které jsou deklarovány opět jako (object):


\begin{lstlisting}
+ variables :
levySenzor, pravySenzor(object).
{dots} (tv{e}lo metody init:)
levySensor = (vozidlo add-sensor at (2.0, 0, -1.5)).
pravySensor = (vozidlo add-sensor at (2.0, 0, 1.5)).
\end{lstlisting}

Následně propojíme senzory s~koly. Kola budou reagovat na vstupy
senzorů. Propojit se mohou například křížem.

\begin{lstlisting}
levySensor link to praveKolo.
pravySensor link to leveKolo.
\end{lstlisting}

Lze také nastavit sílu reakce kol na vstupy senzorů, záporná
hodnota odpuzuje senzor od navigačního světla.

\begin{lstlisting}
levySensor set-bias to .5.
pravySensor set-bias to -0.05.
\end{lstlisting}

Takže nyní lze přistoupit k~rozmístění navigačních světel dle libosti.

\begin{lstlisting}
+ variables :
svetlo1, svetlo2, svetlo3, svetlo4 (object).
{dots} (tv{e}lo metody init:)
svetlo1 = new BraitenbergLight.
svetlo1 move to (6, 1, -6).
svetlo2 = new BraitenbergLight.
svetlo2 move to (6, 1, 6).
svetlo3 = new BraitenbergLight.
svetlo3 move to (-6, 1, -6).
svetlo4 = new BraitenbergLight.
svetlo4 move to (-6,1,6).
\end{lstlisting}

Nebo lze světla rozmístit například do kruhu.

\begin{lstlisting}
+ variables:
n (int).
... (telo metody init:)
for n=0, n{textless}10, n++:
  new BraitenbergLight move to (20 * sin(n * 6.28 / 10),
                                1,
                                20 * cos(n * 6.28 / 10)).
\end{lstlisting}

Přidávat můžeme ještě další objekty, Například zeď jako překážku
k~proražení. Pro definici kvádrů zdi je třeba definovat třídu.

\begin{lstlisting}
Mobile : Brick (aka Bricks) {
  + variables:
    shape (object).

  + to init:
    self set-shape to (new Cube init-with size (1, 1, 1)).
    self enable-physics.
}
\end{lstlisting}

Zeď pak postavíme rozmístěním jednotlivých bloků.

\begin{lstlisting}
blocks = 4 new Bricks.
...
blocks{ 0 } move to ( 65, 0.6, -1.65 ).
blocks{ 1 } move to ( 65, 0.6,  -.55 ).
blocks{ 2 } move to ( 65, 0.6,   .55 ).
blocks{ 3 } move to ( 65, 0.6,  1.65 ).
\end{lstlisting}

Nebo můžeme vytvořit nájezdový skokanský můstek.

\begin{lstlisting}
+ variables :
rampShape (object).
ramp (object).
rampa = new Stationary.
rampa register with-shape rampShape
    at-location ( 50, 1.4, 0 )
    with-rotation [ (0.96596, -0.25869, 0.00000),
         (0.25869, 0.96596, 0.00000),
        (0.00000, 0.00000, 1.00000) ].
\end{lstlisting}

Obě dvě komponenty jsou použity v~ukázkovém projektu
\verb!Demolition.tz!, které je součástí Breve.


\section{Programování simulací v~Pythonu}
\label{sec:programovani-simulaci-v-pythonu}

Od~verze 2.6 je do Breve přidána podpora pro~programování simulací
v~jazyce Python. Jelikož autor chce, aby program fungoval hned po
instalaci, je běhové prostředí se základními knihovnami součásí
programu breve.

Výhodou integrace s~Pythonem je, že lze využívat objekty
oboustranně. Tedy objekty z~Pythonu lze volat ostatními jazyky
a~naopak.

Pro~inkluzi zdrojového kódu Python lze použít \verb!@include! s~cestou
k souboru s~deklaracemi. Je to tedy stejný způsob, jako inkluze
zdrojových kódů steve (viz \autoref{sec:import-rodicovske-tridy}).

\subsection{Deklarace objektů v~Pythonu}
\label{sec:deklarace-objektu-v-pythonu}

Každý vytvořený objekt musí být podtřídou třídy \verb!breve.Object!,
která má zapouzdřenou komunikaci s~breve engine. Objekt níže bude
dostupný pro~engine breve a při každé iteraci simulace bude volána
metoda \verb!iterate! tohoto objektu, pokud je v~simulaci použit.

\begin{lstlisting}[language=Python]
import breve

class PythonBridgeObject( breve.Object ):
    def __init__( self ):
        breve.Object.__init__( self )
        print "Inited Python bridge object"

    def iterate( self ):
        print "Iterating Python bridge object"
\end{lstlisting}

Pro každý projekt pak vytváříme třídu kontroleru, která je podtřídou
vestavěné třídy \verb!breve.Control!.

\begin{lstlisting}[language=Python]
import breve

class myController( breve.Control ):
    def __init__( self ):
        breve.Control.__init__( self )
        self.agent = myAgent()
        print "simulation started"

    def iterate( self ):
        breve.Control.iterate( self )

class myAgent( breve.Mobile ):
        def __init__( self ):
                breve.Mobile.__init__( self )
                print "created agent"

        def iterate( self ):
                # make the agent do something
                # interesting here
                breve.Control.iterate( self )

# instantiate the controller to initialize the simulation
myController()
\end{lstlisting}

Více o využití Pythonu v~breve
na~\url{http://spiderland.org/node/1504}.


\section{Literatura}
\label{sec:literatura}

\begin{thebibliography}{9}

\bibitem[Dong2014]{dong2014}
  Dong, Anguo,
  \emph{MPM Simulation Tools - Breve and Swarm}.
  [online] Computer Science Department University of Clagary.
  [Citováno \date{1. 4. 2014}].
  Dostupné z: \url{http://pages.cpsc.ucalgary.ca/~jacob/Courses/Winter2005/CPSC565-607/Slides/Students/Grads/08-Breve-Swarm.pdf}.

\bibitem[Klein2004]{Klein2004}
  Klein, Jonathan.
  \emph{Building Braitenberg Vehicles in breve: a tutorial The breve Simulation Environment}.
  [online] \date{22. 6. 2004}.
  [Citováno \date{1. 4. 2104}]
  Dostupné z: \url{http://www.spiderland.org/BraitenbergTutorial.pdf}

\bibitem[Klein2006]{Klein2006}
  Klein, Jonathan.
  \emph{The breve Simulation Environment Documentation}.
  [online] \date{17. 9. 2006}.
  [Citováno \date{1. 4. 2014}].
  Dostupné z: \url{http://spiderland.org/breve/documentation.php}.

\end{thebibliography}

\end{document}
